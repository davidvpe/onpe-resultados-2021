const axios = require('axios').default

let mensaje = 'asdasdasd'

async function run() {
    while (true) {
        var interval = 1000 * 60 * (Math.random() * 5 + 5)

        await axios
            .get(
                'https://api.resultadossep.eleccionesgenerales2021.pe/results/10/000000?name=param'
            )
            .then((res) => res.data)
            .then((data) => {
                let porcentaje = data.generals.actData.POR_AVANCE
                let perulibreResults = data.results.find(
                    (p) => p.C_CODI_AGRUPOL === '00000014'
                )
                let fuerzapopularResults = data.results.find(
                    (p) => p.C_CODI_AGRUPOL === '00000007'
                )

                let nuevo_mensaje =
                    `ONPE al ${porcentaje} %\n` +
                    `PERU LIBRE: ${perulibreResults.POR_VALIDOS} %\n` +
                    `FUERZA POPULAR: ${fuerzapopularResults.POR_VALIDOS} %\n`

                if (mensaje != nuevo_mensaje) console.log(nuevo_mensaje)
                mensaje = nuevo_mensaje
            })

        await new Promise((res, rej) => {
            setTimeout(() => {
                res()
            }, interval)
        })
    }
}

run()
